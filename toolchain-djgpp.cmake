# See also:
# https://github.com/Wohlstand/libADLMIDI/blob/master/cmake/djgpp/toolchain-djgpp.cmake
# https://github.com/badlogic/dos-dev-template/blob/main/tools/toolchain-djgpp.cmake

# CMAKE_SYSTEM_NAME=DOS forces .lib extension for libraries.
set (CMAKE_SYSTEM_NAME Generic)

set (DJGPP TRUE)

# specify the cross compiler
set (CMAKE_C_COMPILER /usr/local/djgpp/bin/i586-pc-msdosdjgpp-gcc)
set (CMAKE_CXX_COMPILER /usr/local/djgpp/bin/i586-pc-msdosdjgpp-g++)

# where is the target environment
set (CMAKE_FIND_ROOT_PATH /usr/local/djgpp)

# search for programs in the build host directories
set (CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
set (CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set (CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set (CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
