/*
This file is part of sdgi2sf2.

sdgi2sf2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

sdgi2sf2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with sdgi2sf2.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BOX_H
#define BOX_H

#include <math.h>
#include <stdbool.h>
#include <stddef.h>

static const double box_mean = 0.3149802624737183; // == exp(-20 * (log(2) / 12))

struct box_mb
{
	// y = m * x + b
	double m, b;
};

struct box_xmb
{
	double x;
	struct box_mb mb;
};

struct box_line_range
{
	const struct box_xmb *seg_src;
	const struct point *seg_dst;

	bool has_x1_0, has_x1_1;
	double x0, x1;
};

struct box_vtbl
{
	// Protip: ∫ a,b f(m*x)/x dx = ∫ m*a,m*b f(x)/x dx

	// solve(f(v,u)-v[0]=s.m*u+s.b,u)
	size_t (*u)(double lC, const struct box_mb s, const double *v, double *u, double (*g)[4]);

	// ∫ f(v,u)-v[0] du
	double (*i)(double lC, double u, const double *v);

	// ∫ (f(v,u)-v[0])/u du
	// If v[0] isn't real, do ∫ f(v,u)/u du and optionally factor out any log(u) terms into v[0].
	double (*idiv)(double lC, double u, const double *v, double *grad);

	// Should always be true: diff(i(lC,u,v),u)*u+v[0] = f(v,u)
};

// f(v,u) = f_y(y,t.m*u+t.b)
// y: The dest segment scaled to unit distance x.
// TODO: Overlapping segments very likely need an box_mb y and an box_mb t for each segment.
// Imagine two segments of cos(%pi*t).
typedef const struct box_vtbl *(*box_v)(double lC, double y0, double y1, struct box_mb t, double *v, double (*g)[4]);

// (Private.)
double box2(const struct box_vtbl *vtbl, double lC, struct box_mb mb_s0, double x0, double x1, const double *v, double *grad);
void box0(const struct box_vtbl *vtbl, double lC, struct box_mb mb_s0, struct box_mb mb_px, const double *v, double *grad);

double box_sqr(box_v box_v, double lC, const struct box_line_range *pos, double *grad);

static inline double box_warp(double x)
{
	return sqrt(x);
}

static inline double box_warp_inv(double x)
{
	return x * x;
}

static inline double box_warp_inv_sub(double x, double y)
{
	// See the "Cancellation" section in "What Every Computer Scientist Should Know About Floating-Point Arithmetic"
	// https://docs.oracle.com/cd/E19957-01/806-3568/ncg_goldberg.html
	// return x * x - y * y;
	return (x - y) * (x + y);
}

static inline double box_warp_inv_diff(double x)
{
	return 2 * x;
}

/*
static inline double box_warp(double x)
{
	return x;
}

static inline double box_warp_inv(double x)
{
	return x;
}

static inline double box_warp_inv_sub(double x, double y)
{
	return x - y;
}

static inline box_warp_inv_diff(double x)
{
	return 1;
}
*/

#endif
