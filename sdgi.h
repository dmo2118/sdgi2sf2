/*
This file is part of sdgi2sf2.

sdgi2sf2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

sdgi2sf2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with sdgi2sf2.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SDGI_H
#define SDGI_H

#include <inttypes.h>
#include <math.h>
#include <stdbool.h>

static inline uint32_t big_endian_32(uint32_t x)
{
	return __builtin_bswap32(x);
}

static inline uint16_t big_endian_16(uint16_t x)
{
	return __builtin_bswap16(x);
}

#if __DJGPP__
#	define static_assert _Static_assert
#endif

#define ASSERT_OFFSET(s, f, n) static_assert(offsetof(struct s, f) == (n), #s "." #f " != " #n);

// https://openmuse.org/noncpl/DLS_ICMC.PDF

typedef uint16_t sdgi_id;

typedef uint32_t sdgi_wave_head;

struct __attribute__((packed)) sdgi_wave_body
{
	uint8_t flags;
	uint16_t pitch;
	uint32_t start;
	uint32_t start_loop;
	uint32_t end_loop;
	uint32_t end;
	uint16_t alt_wave;
};

ASSERT_OFFSET(sdgi_wave_body, flags,        0x00);
ASSERT_OFFSET(sdgi_wave_body, pitch,        0x01);
ASSERT_OFFSET(sdgi_wave_body, start,        0x03);
ASSERT_OFFSET(sdgi_wave_body, start_loop,   0x07);
ASSERT_OFFSET(sdgi_wave_body, end_loop,     0x0b);
ASSERT_OFFSET(sdgi_wave_body, end,          0x0f);
ASSERT_OFFSET(sdgi_wave_body, alt_wave,     0x13);

struct __attribute__((packed)) sdgi_wave_redirect
{
	uint8_t flags;
	uint8_t shift;
};

ASSERT_OFFSET(sdgi_wave_redirect, flags,    0x00);
ASSERT_OFFSET(sdgi_wave_redirect, shift,    0x01);

static const uint8_t sdgi_wave_flag_redirect = 1 << 7;
static const uint8_t sdgi_wave_flag_dir = 1 << 6;
static const uint8_t sdgi_wave_flag_alt_wave = 1 << 5;
static const uint8_t sdgi_wave_flag_bpe = 1 << 4;
static const uint8_t sdgi_wave_flag_lpe = 1 << 3;
static const uint8_t sdgi_wave_flag_cmpd = 1 << 1;

struct __attribute__((packed)) sdgi_region_lfo
{
	uint8_t _0;
	uint8_t _1;
	uint8_t _2;
	uint8_t _3;
	uint8_t _4;
	uint8_t _5;
	uint8_t _6;
	uint8_t _7;
	uint8_t _8;
};

struct __attribute__((packed)) sdgi_region_ramp
{
	uint8_t time[6];
	int8_t level[5];
	uint8_t velocity_scale0;
	uint8_t velocity_attack;
	uint8_t semitone_scale;
};

ASSERT_OFFSET(sdgi_region_ramp, time,               0x00);
ASSERT_OFFSET(sdgi_region_ramp, level,              0x06);
ASSERT_OFFSET(sdgi_region_ramp, velocity_scale0,    0x0B);
ASSERT_OFFSET(sdgi_region_ramp, velocity_attack,    0x0C);
ASSERT_OFFSET(sdgi_region_ramp, semitone_scale,     0x0D);

struct __attribute__((packed)) sdgi_region // sizeof == 0x46
{
	int16_t pitch;
	uint8_t vol;
	uint8_t _3;
	uint16_t wave;
	uint8_t flags;
	uint8_t pitch_params;
	int8_t pitch_param_scale1;
	int8_t pitch_param_scale2;
	uint8_t vol_param;
	int8_t vol_param_scale;
	uint8_t k_params;
	int8_t k1_param_scale;
	int8_t k1_semitone_scale;
	int8_t k1;
	int8_t k2_param_scale;
	int8_t k2_semitone_scale;
	int8_t k2;

	char unknown[0x18 - 0x13];

	struct sdgi_region_ramp ramp[2];
	struct sdgi_region_lfo lfo[2];
};

ASSERT_OFFSET(sdgi_region, pitch,               0x00);
ASSERT_OFFSET(sdgi_region, vol,                 0x02);
ASSERT_OFFSET(sdgi_region, wave,                0x04);
ASSERT_OFFSET(sdgi_region, flags,               0x06);
ASSERT_OFFSET(sdgi_region, pitch_params,        0x07);
ASSERT_OFFSET(sdgi_region, pitch_param_scale1,  0x08);
ASSERT_OFFSET(sdgi_region, pitch_param_scale2,  0x09);
ASSERT_OFFSET(sdgi_region, vol_param,           0x0a);
ASSERT_OFFSET(sdgi_region, vol_param_scale,     0x0b);
ASSERT_OFFSET(sdgi_region, k_params,            0x0c);
ASSERT_OFFSET(sdgi_region, k1_param_scale,      0x0d);
ASSERT_OFFSET(sdgi_region, k1_semitone_scale,   0x0e);
ASSERT_OFFSET(sdgi_region, k1,                  0x0f);
ASSERT_OFFSET(sdgi_region, k2_param_scale,      0x10);
ASSERT_OFFSET(sdgi_region, k2_semitone_scale,   0x11);
ASSERT_OFFSET(sdgi_region, k2,                  0x12);
ASSERT_OFFSET(sdgi_region, ramp[0],             0x18);
ASSERT_OFFSET(sdgi_region, ramp[1],             0x26);
ASSERT_OFFSET(sdgi_region, lfo[0],              0x34);
ASSERT_OFFSET(sdgi_region, lfo[1],              0x3d);

enum
{
	sdgi_region_flag_no_sustain = 1 << 2,
	sdgi_region_flag_always_reset_envelope = 1 << 2,
	sdgi_region_flag_lp3 = 1 << 4,
	sdgi_region_flag_lp4 = 1 << 5,
};

struct __attribute__((packed)) sdgi_instrument
{
	uint16_t region_id;
	int16_t pitch;
	uint8_t volume;
	uint8_t _5;
	uint8_t melodic_lo_drum_exclusive;
	uint8_t melodic_hi;
};

const uint8_t sdgi_instrument_volume_present = 1 << 7;
const uint8_t sdgi_melodic_lo_velocity_param = 1 << 7;
const uint8_t sdgi_melodic_hi_volume_boost = 1 << 7;
const uint8_t sdgi_instrument_volume_boost = 12;

const uint16_t sdgi_instrument_region_mask = 0x3ff;
const uint8_t sdgi_instrument_mask = 0x7f;

#define sdgi_melodic_instruments 4

const uint8_t sdgi_drum_kit_base = 21;

const uint8_t sdgi_drum_kit_instruments = 88;

static inline bool sdgi_block_ok(const void *block)
{
	const uint32_t *sdgi = block;
	return sdgi[0] == big_endian_32(0x53444749) && sdgi[1] == big_endian_32(1);
}

static inline size_t sdgi_wave_redirect_count(const struct sdgi_wave_redirect *redirect)
{
	assert(redirect->flags & sdgi_wave_flag_redirect);
	assert(redirect->shift < 8);
	return 128 >> redirect->shift;
}

static inline uint8_t sdgi_param1(uint8_t params)
{
	return params >> 4;
}

static inline uint8_t sdgi_param2(uint8_t params)
{
	return params & 0xf;
}

static double sdgi_pitch_param_scale(double x)
{
	static const double Q = 0x4081 / 48.0;
	if(x < -64)
		x += 63;
	else if(x > 64)
		x -= 63;
	else
		x *= 1.0 / 64.0;
	return Q * x;
}

const uint32_t sdgi_block_size = 0x100000;
const uint16_t sdgi_eof = 0xffff;

#endif
