/*
This file is part of sdgi2sf2.

sdgi2sf2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

sdgi2sf2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with sdgi2sf2.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef IO_RESULT_H
#define IO_RESULT_H

#if _WIN32

#if !defined(_UNICODE) && UNICODE
#	define _UNICODE 1
#endif

#include <tchar.h>
#include <windows.h>

typedef DWORD io_result;

enum
{
	io_result_no_memory = ERROR_NOT_ENOUGH_MEMORY
};

const _TCHAR *io_result_new_str(int error);
void io_result_delete(const _TCHAR *str);

// Not currently a better place to put getopt().
extern _TCHAR *optarg;
extern int optind;

// DOS/Windows-appropriate behavior.
int getopt(int argc, _TCHAR **argv, const _TCHAR *optstring);

#else

#include <errno.h>
#include <string.h>

typedef int io_result;
typedef char _TCHAR;

#define _TEXT
#define _ftprintf fprintf
#define _tmain main
#define _tprintf printf

enum
{
	io_result_no_memory = ENOMEM
};

static inline const _TCHAR *io_result_new_str(int error)
{
	return strerror(error);
}

static inline void io_result_delete(const _TCHAR *str)
{
}

#endif

#endif
