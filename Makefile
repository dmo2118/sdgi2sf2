CFLAGS=-O3 -g -Wno-unused-function -Wall -D_LARGEFILE64_SOURCE
LDLIBS=-lnlopt `pkg-config --libs gsl`
# LDFLAGS=-s

sdgi2sf2: \
	sdgi2sf2.o \
	version.o \
	getopt.o \
	io_file.o \
	io_mem.o \
	io_mmap.o \
	io_result.o \
	riff.o \
	envelope_fit.o \
	box.o \
	box_intg.o \
	plot_bezier.o \
	point.o \
	lambert_w_2.o

git_head != [ -f .git/HEAD ] && echo .git/HEAD

version.c: $(git_head)
	{ \
		if version="$$(git show --format=%H -s)"; \
		then \
			version="$$(printf %.8s "$$version")"; \
		else \
			version="$$(pwd | sed -E -n 's/.*-release_(.{8})$$/\1/p')"; \
			if [ -z "$$version" ]; \
			then \
				version=unknown ; \
			fi; \
		fi; \
		\
		printf \
			'#include "version.h"\n\nconst char version[] = "%s";\nconst ptrdiff_t version_size = %d;\n' \
			"$${version}" \
			$${#version}; \
	} > version.c

clean:
	rm -f *.o sdgi2sf2
