/*
This file is part of sdgi2sf2.

sdgi2sf2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

sdgi2sf2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with sdgi2sf2.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef LAMBERT_W_2
#define LAMBERT_W_2

#include <math.h>

static inline unsigned lambert_w_branches(double b, double a)
{
	// This breaks down for negative denormals near zero if -lm uses a floating-point unit different from this code, and one has
	// denormals-as-zero turned on and the other does not, e.g. by combining -ffast-math (which turns on DAZ and FZ in MXCSR)
	// with -mfpmath=387 on x86-64).
	if(b >= 0)
		return 1;
	if(log(-b) + a <= -1)
		return 2;
	return 0;
}

// W(b*exp(a))-a;
// ba = b-a
// solve(b=(w+a)*exp(w),w)
double lambert_w0_2(double b, double a, double ba);
double lambert_wm1_2(double b, double a, double ba);

#endif

