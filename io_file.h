/*
This file is part of sdgi2sf2.

sdgi2sf2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

sdgi2sf2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with sdgi2sf2.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef IO_FILE_H
#define IO_FILE_H

#include "io_file_raw.h"
#include "io_result.h"

#include <stdbool.h>

typedef unsigned long long io_file_size;

struct io_file
{
	io_file_raw _f;
	io_file_size _pos;

	void *_buffer;
	io_file_size _offset;
	size_t _size;
};

io_result io_file_new(struct io_file *self, const _TCHAR *path);
io_result io_file_delete(struct io_file *self);

io_result io_file_write(struct io_file *self, const void *data, size_t size);

static inline io_file_size io_file_seek(struct io_file *self, io_file_size pos)
{
	io_file_size prev_pos = self->_pos;
	self->_pos = pos;
	return prev_pos;
}

static inline io_file_size io_file_tell(struct io_file *self)
{
	return self->_pos;
}

void *io_file_write_begin(struct io_file *self, size_t size);

static inline void io_file_write_rollback(struct io_file *self)
{
}

io_result io_file_write_commit(struct io_file *self, size_t size);

#endif
